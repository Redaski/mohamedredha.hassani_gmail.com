print("hello")
import random
import math

Hôpitaux = {'H1': { 'crit1' : 405, 'crit2' : 36,
                   'crit3': 16, 'crit4': 80, 'crit5' : 20}, 
            'H2': { 'crit1' : 608, 'crit2' : 60,
                   'crit3': 15, 'crit4': 105, 'crit5' : 27},
            'H3': { 'crit1' : 310, 'crit2' : 40,
                   'crit3': 18, 'crit4': 70, 'crit5' : 15},
            'H4': { 'crit1' : 290, 'crit2' : 35,
                   'crit3': 4, 'crit4': 12, 'crit5' : 25},
            'H5': { 'crit1' : 528, 'crit2' : 75,
                   'crit3': 55, 'crit4': 154, 'crit5' : 15},
            'H6': { 'crit1' : 206, 'crit2' : 10,
                   'crit3': 6, 'crit4': 18, 'crit5' : 18},
            'H7': { 'crit1' : 367, 'crit2' : 67,
                   'crit3': 38, 'crit4': 100, 'crit5' : 10},
            'H8': { 'crit1' : 134, 'crit2' : 34,
                   'crit3': 5, 'crit4': 75, 'crit5' : 9},
            'H9': { 'crit1' : 806, 'crit2' : 22,
                   'crit3': 12, 'crit4': 27, 'crit5' : 19},
            'H10': { 'crit1' : 381, 'crit2' : 4,
                   'crit3': 38, 'crit4': 120, 'crit5' : 12}}

Profils = {'b4': { 'crit1' : 3000, 'crit2' : 100,
                   'crit3': 100, 'crit4': 200, 'crit5' : 100}, 
            'b3': { 'crit1' : 500, 'crit2' : 28,
                   'crit3': 37, 'crit4': 50, 'crit5' : 30},
            'b2': { 'crit1' : 325, 'crit2' : 17,
                   'crit3': 13, 'crit4': 34, 'crit5' : 28},
            'b1': { 'crit1' : 0, 'crit2' : 0,
                   'crit3': 0, 'crit4': 0, 'crit5' : 0}}

mpoid = {'crit1' :0.2, 'crit2' :0.15, 'crit3':0.25, 'crit4':0.15 ,'crit5':0.25}

#calcul de la somme des poids des critères d'un hopital en comparaison avec un profil
def classe(chu,pro):
    a = 0 
    for b in chu :
        if chu[b] >= pro[b]:
            a += mpoid[b]
    return a>=0.5

#listing et classification ordinale des différents hôpitaux du classeur r en 3 liste, chacune représentant une catégorie 
def portail(r):
    TB = []
    B = []
    M = []
    for b in r :
        compt =0
        for t in Profils :
            if classe(r[b],Profils[t]) == True :
                compt += 1
        if compt == 1:
            M.append(b)
        if compt == 2:
            B.append(b)
        if compt == 3:
            TB.append(b)
    print('Hôpitaux très bien : ',TB)
    print('Hôpitaux bien : ',B)
    print('Hôpitaux moyen : ',M)
    

#génératon d'une matrice d'au moins 20 hôpitaux selon votre input (1/5 au moins en TB et 1/3 au moins en moyen)
def classeur():
    banque = {} 
    n = input("Combien d'hôpitaux voulez-vous générer ?")
    n = int(n)
    q = random.randrange(math.ceil((7*n)/15))
    s = random.randrange(math.ceil(q),math.ceil((7*n)/15))
    for m in range(n):
        banque['H'+str(m+1)] = {}
    for p in range(math.ceil(n/5)+q) :
        banque['H'+str(p+1)]['crit1'] = random.randrange(Profils['b4']["crit1"])
        banque['H'+str(p+1)]['crit2'] = random.randrange(Profils['b4']["crit2"])
        banque['H'+str(p+1)]['crit3'] = random.randrange(Profils['b3']['crit3'],Profils['b4']["crit3"])
        banque['H'+str(p+1)]['crit4'] = random.randrange(Profils['b4']["crit4"])
        banque['H'+str(p+1)]['crit5'] = random.randrange(Profils['b3']['crit5'],Profils['b4']["crit5"])
    for p in range(math.ceil(n/5)+q,math.ceil((n/5)+(n/3))+s) :
        banque['H'+str(p+1)]['crit1'] = random.randrange(Profils['b2']["crit1"])
        banque['H'+str(p+1)]['crit2'] = random.randrange(Profils['b2']["crit2"])
        banque['H'+str(p+1)]['crit3'] = random.randrange(Profils['b1']['crit3'],Profils['b2']["crit3"])
        banque['H'+str(p+1)]['crit4'] = random.randrange(Profils['b2']["crit4"])
        banque['H'+str(p+1)]['crit5'] = random.randrange(Profils['b1']['crit5'],Profils['b2']["crit5"])
    for p in range(math.ceil((n/5)+(n/3))+s,n) :
        banque['H'+str(p+1)]['crit1'] = random.randrange(Profils['b3']["crit1"])
        banque['H'+str(p+1)]['crit2'] = random.randrange(Profils['b3']["crit2"])
        banque['H'+str(p+1)]['crit3'] = random.randrange(Profils['b2']['crit3'],Profils['b3']["crit3"])
        banque['H'+str(p+1)]['crit4'] = random.randrange(Profils['b3']["crit4"])
        banque['H'+str(p+1)]['crit5'] = random.randrange(Profils['b2']['crit5'],Profils['b3']["crit5"])                                                                        
    return banque


portail(Hôpitaux)
                
print(classeur()) 

portail(classeur())  #classification ordinale de la matrice fraichement générée     


